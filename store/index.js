export const state = () => ({
	colors: [
		'primary',
		'secondary',
		'warning',
		'danger',
		'black',
		'light-grey',
		'white'
	],
	styles: ['', 'ghost-', 'flat-'],
	heights: [1, 2, 3, 4, 5, 6]
})

export const getters = {
	getColor(state) {
		return state.colors
	},
	getStyle(state) {
		return state.styles
	},
	getHeights(state) {
		return state.heights
	}
}

export const mutations = {}
